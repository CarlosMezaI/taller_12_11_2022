package com.meza.taller12_10_2022;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;
import java.util.List;

public class MainActivity2 extends AppCompatActivity {

RecyclerView rvListas;
Adaptador miAdaptador;
Button mapa;
Button calculadora;
Button equipos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

    rvListas=findViewById(R.id.rvlista);
    rvListas.setLayoutManager(new LinearLayoutManager(this));
    mapa=(Button)findViewById(R.id.btnmapa);
    calculadora=(Button)findViewById(R.id.btncalculadora);
    equipos=(Button)findViewById(R.id.btnequipos);


    mapa.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent mapa = new Intent(MainActivity2.this, ActivityMapa.class);
            startActivity(mapa);
        }
    });

    calculadora.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent calculadora = new Intent(MainActivity2.this, ActivityCalculadora.class);
            startActivity(calculadora);
        }
    });

    equipos.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent equipos = new Intent(MainActivity2.this, ActivityEquipos.class);
            startActivity(equipos);
        }
    });


        List<String> misDatosPer=new ArrayList<>();
        misDatosPer.add("CARLOS HUMBERTO");
        misDatosPer.add("MEZA INFANTE");
        misDatosPer.add("TALLER 3");


        miAdaptador=new Adaptador(misDatosPer);
        rvListas.setAdapter(miAdaptador);


    }
}