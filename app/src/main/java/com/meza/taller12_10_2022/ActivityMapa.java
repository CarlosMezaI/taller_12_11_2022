package com.meza.taller12_10_2022;

import static android.content.ContentValues.TAG;

import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import org.osmdroid.api.IMapController;
import org.osmdroid.config.Configuration;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.Marker;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;



public class ActivityMapa extends AppCompatActivity {

    MapView mapa;
    IMapController mapaControlador;
    MyLocationNewOverlay miUbicación;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa);

        mapa = (MapView) findViewById(R.id.map);

        Context ctx = getApplicationContext();
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));

        mapa.setTileSource(TileSourceFactory.MAPNIK);

        mapa.setMultiTouchControls(true);

        String[] permisos = new String[]{
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permisos, 1);

            mapaControlador = mapa.getController();

            miUbicación = new MyLocationNewOverlay(new GpsMyLocationProvider(this), mapa);
            miUbicación.enableMyLocation();
            mapa.getOverlays().add(miUbicación);

            miUbicación.runOnFirstFix(
                    () -> {
                        runOnUiThread(
                                () -> {
                                    mapaControlador.setZoom((double) 10);
                                            mapaControlador.setCenter(miUbicación.getMyLocation());
                                    Log.d(TAG, "Ubicado" + miUbicación.getMyLocation().getLongitude() + miUbicación.getMyLocation().getLatitude());
                                });
                    }
            );
        }

    }
}
