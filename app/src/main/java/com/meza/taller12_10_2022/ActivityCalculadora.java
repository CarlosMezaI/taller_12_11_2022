package com.meza.taller12_10_2022;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;

public class ActivityCalculadora extends AppCompatActivity implements View.OnClickListener{

    //1.Crear los objetos que se relacionaran con los botones del layout

    MaterialButton boton1, boton2, boton3, boton4, boton5,boton6,boton7, boton8, boton9, boton0, boton_c, boton_parentesisI, boton_parentesisD, boton_AC;
    TextView expresion;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        //se asocia al textview que esta en el layout
        expresion=findViewById(R.id.expresion);

        //2.Relacionar el objeto boton1 con el elemento cuyo id sea boton_1

        boton1=findViewById(R.id.botton_1);
        boton2=findViewById(R.id.botton_2);
        boton3=findViewById(R.id.botton_3);
        boton4=findViewById(R.id.botton_4);
        boton5=findViewById(R.id.botton_5);
        boton6=findViewById(R.id.botton_6);
        boton7=findViewById(R.id.botton_7);
        boton8=findViewById(R.id.botton_8);
        boton9=findViewById(R.id.botton_9);
        boton0=findViewById(R.id.botton_0);
        boton_c=findViewById(R.id.botton_C);
        boton_parentesisI=findViewById(R.id.botton_parentesis_I);
        boton_parentesisD=findViewById(R.id.botton_parentesis_D);
        boton_AC=findViewById(R.id.botton_ac);

        //3.Convertir el boton1 en un listener

        boton1.setOnClickListener(this);
        boton2.setOnClickListener(this);
        boton3.setOnClickListener(this);
        boton4.setOnClickListener(this);
        boton5.setOnClickListener(this);
        boton6.setOnClickListener(this);
        boton7.setOnClickListener(this);
        boton8.setOnClickListener(this);
        boton9.setOnClickListener(this);
        boton0.setOnClickListener(this);
        boton_c.setOnClickListener(this);
        boton_parentesisI.setOnClickListener(this);
        boton_parentesisD.setOnClickListener(this);
        boton_AC.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        // to do aqui va a la lógica cuando se da click
        //1. crear objeto que reprsente al boton al cual se le ha hecho click

        MaterialButton boton=(MaterialButton) view;
        String text = boton.getText().toString();
        String textAnterior = expresion.getText().toString();

        //2.colocar en la expresión
        if(text ==boton_c.getText().toString() || text == boton_AC.getText().toString()){
            this.expresion.setText("");
        }else {
            this.expresion.setText(textAnterior + text);
        }

    }
}