package com.meza.taller12_10_2022;

import androidx.appcompat.app.AppCompatActivity;

        import android.content.Intent;
        import android.os.Bundle;
        import android.webkit.WebView;
        import android.webkit.WebViewClient;

public class ListaEquipos extends AppCompatActivity {

    WebView webview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_equipos);

        Intent intent = getIntent();

        String equipo =intent.getStringExtra("equipo");

        equipo.replaceAll("","_");

        webview=findViewById(R.id.webView);
        String fullUrl="https://es.wikipedia.org/wiki/" + equipo;
        webview.setWebViewClient(new WebViewClient());
        webview.loadUrl(fullUrl);
    }
}