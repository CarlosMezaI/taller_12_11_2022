package com.meza.taller12_10_2022;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class ActivityEquipos extends AppCompatActivity implements AdapterView.OnItemClickListener {

    ListView equipos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_equipos);

        equipos=findViewById(R.id.equipos);

        String[] misEquipos=getResources().getStringArray(R.array.equipos);



        ArrayAdapter equiposAdapter= new ArrayAdapter(this, R.layout.equipo,misEquipos );

        equipos.setAdapter(equiposAdapter);
        equipos.setOnItemClickListener((AdapterView.OnItemClickListener) this);


    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

        String equipoSeleccionado = ((TextView)view).getText().toString();

        //create a short Toast message
        Toast.makeText(getBaseContext(), equipoSeleccionado, Toast.LENGTH_SHORT).show();

        //alternatively we can create a Snackbar instead of a Toast
        //android.R.id.content finds the root element of current view
        Snackbar.make(findViewById(android.R.id.content),equipoSeleccionado, Snackbar.LENGTH_LONG).show();

        Intent intent = new Intent(getBaseContext(), ListaEquipos.class);
        intent.putExtra("equipo", equipoSeleccionado);
        startActivity(intent);

    }
}