package com.meza.taller12_10_2022;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class Adaptador extends RecyclerView.Adapter <Lista> {


    List<String> listas;

    public Adaptador(List<String> datos){
        listas=datos;

    }


    @NonNull
    @Override
    public Lista onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View miCargador;

        miCargador=LayoutInflater.from(parent.getContext()).inflate(R.layout.lista,parent,false);
        return  new Lista(miCargador);
    }

    @Override
    public void onBindViewHolder(@NonNull Lista holder, int position) {

        String misDatos=listas.get(position);
        holder.tvLista.setText(misDatos);

    }

    @Override
    public int getItemCount() {
        if(listas!=null) {
            return listas.size();
        }else{
            return 0;
        }
    }
}
