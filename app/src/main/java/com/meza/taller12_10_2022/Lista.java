package com.meza.taller12_10_2022;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class Lista  extends RecyclerView.ViewHolder {

    TextView tvLista;

    public Lista(@NonNull View itemView) {
        super(itemView);

        tvLista=itemView.findViewById(R.id.tvtlista);
    }
}
