package com.meza.taller12_10_2022;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    Button btnActividades;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnActividades=(Button)findViewById(R.id.rvactividades);

        btnActividades.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent btnActividades = new Intent(MainActivity.this, MainActivity2.class);
                startActivity(btnActividades);
            }
        }));
    }

}